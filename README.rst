==========================
Format Specification Suite
==========================

:format: DATEX II Measured Data Publication - Weather Data
:uri: cz-ndic_d2-weather-data-v1.0

This repository provides tools and files for given format:

- schema
- sample(s)
- documentation
- test suite
- unified `tox` based interface for related tools

About described format
======================

For all details, see `FORMAT.yaml`.

Using provided tools
====================

For all details, see `tox.rst`.

Changelog
=========

version 1.0.0 

- Innitial format and documentation